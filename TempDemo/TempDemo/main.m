//
//  main.m
//  TempDemo
//
//  Created by ganeshu on 10/08/18.
//  Copyright © 2018 Epicomm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
