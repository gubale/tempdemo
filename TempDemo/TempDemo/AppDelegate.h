//
//  AppDelegate.h
//  TempDemo
//
//  Created by ganeshu on 10/08/18.
//  Copyright © 2018 Epicomm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

